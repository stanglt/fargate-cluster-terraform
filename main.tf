provider "aws" {
  region  = "eu-central-1"
  version = "~> 2.0"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"

  tags = {
    user = var.developer
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "public" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true

  tags = {
    user = var.developer
  }
}

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

    tags = {
    user = var.developer
  }
}
 
resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}
 
resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

resource "aws_security_group" "ecs_tasks" {
  name   = "${var.developer}-sg-task-fargate-test"
  vpc_id = aws_vpc.main.id
 
  ingress {
   protocol         = "tcp"
   from_port        = 80
   to_port          = 9000
   cidr_blocks      = ["0.0.0.0/0"]
   ipv6_cidr_blocks = ["::/0"]
  }
 
  egress {
   protocol         = "-1"
   from_port        = 0
   to_port          = 0
   cidr_blocks      = ["0.0.0.0/0"]
   ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    user = var.developer
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "${var.developer}-ecsTaskExecutionRole"
 
  assume_role_policy = <<EOF
{
 "Version": "2012-10-17",
 "Statement": [
   {
     "Action": "sts:AssumeRole",
     "Principal": {
       "Service": "ecs-tasks.amazonaws.com"
     },
     "Effect": "Allow",
     "Sid": ""
   }
 ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_ecr_repository" "main" {
  name                 = "post-api"
  image_tag_mutability = "MUTABLE"

  tags = {
    user = var.developer
  }
}

resource "aws_ecr_lifecycle_policy" "main" {
  repository = aws_ecr_repository.main.name
 
  policy = jsonencode({
   rules = [{
     rulePriority = 1
     description  = "keep last 2 images"
     action       = {
       type = "expire"
     }
     selection     = {
       tagStatus   = "any"
       countType   = "imageCountMoreThan"
       countNumber = 2
     }
   }]
  })
}

resource "aws_ecs_cluster" "main" {
  name = "${var.developer}-fargate-test-cluster"

  tags = {
    user = var.developer
  }
}

resource "aws_ecs_task_definition" "main" {
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  family                   = "${var.developer}-fargate-test"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  #task_role_arn            = aws_iam_role.ecs_task_role.arn
  container_definitions = jsonencode([{
    name  = "fargate-test-container"
    image = "${var.docker_image}"
    portMappings = [
      {
        "containerPort" : 80,
        "hostPort" : 80,
        "protocol" : "tcp"
      },
      {
        "containerPort" : 443,
        "hostPort" : 443,
        "protocol" : "tcp"
      }
    ]
  }])

  tags = {
    user = var.developer
  }
}

resource "aws_ecs_service" "main" {
  name            = "${var.developer}-fargate-test-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.main.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [
      aws_security_group.ecs_tasks.id
    ]
    subnets = [
      aws_subnet.public.id,
    ]
    assign_public_ip = true
  }

  tags = {
    user = var.developer
  }
}
