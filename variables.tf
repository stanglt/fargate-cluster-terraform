variable "developer" {
    type = string
}

variable "docker_image" {
    type    = string
    default = "nginxdemos/hello"
}